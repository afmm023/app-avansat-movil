import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ActivacionPage } from '../activacion/activacion';


@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  setActivacion(){

    this.navCtrl.setRoot(ActivacionPage, null, {animate: true,animation: 'ios-transition', direction: 'forward'});

  }


}
