import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtapacarguePage } from './etapacargue';

@NgModule({
  declarations: [
    EtapacarguePage,
  ],
  imports: [
    IonicPageModule.forChild(EtapacarguePage),
  ],
  exports: [
    EtapacarguePage
  ]
})
export class EtapacarguePageModule {}
