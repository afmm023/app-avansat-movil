import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import * as moment from 'moment';

import { DespachosPage } from '../despachos/despachos';
import { NovedadespecialPage } from '../novedadespecial/novedadespecial';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';
import { BdProvider } from '../../providers/bd/bd';


@IonicPage()
@Component({
  selector: 'page-etapacargue',
  templateUrl: 'etapacargue.html',
})
export class EtapacarguePage {

  public estado: any;
  public etapa: any;
  public despacho: any;
  public despachoid: any;
  public usuario: any;
  public novedad: any;
  public latitud: any;
  public longitud: any;
  public direccion: any;
  public opciones: any;
  public datosNovedad: any;
  public manifiesto: any;

  constructor(
    public navCtrl: NavController,
    private loader: LoadingController,
    public parametrosURL: NavParams,
    private storage: NativeStorage,
    private geolocation: Geolocation,
    public api:  ApiFucntionsProvider,
    private network: Network,
    private bd: BdProvider,
    public app: AppFunctionsProvider
  ) {

    this.storage.getItem('usuario')
    .then((result) => {

          this.usuario = result;

    }, error => console.log(error));

  }

  ionViewWillEnter(){

      let load = this.loader.create({

        content: 'Cargando...'

      });

      load.present();
      this.etapa = this.parametrosURL.get('etapa');
      this.despacho = this.parametrosURL.get('despacho');
      this.despachoid = this.parametrosURL.get('despachoid');
      this.manifiesto = this.parametrosURL.get('manifiesto');
      this.verificaEstado();
      this.getGeolocalizacion();
      load.dismiss();

  }

  verificaEstado(){

      var estadoactual = this.parametrosURL.get('estado');

      switch(estadoactual){
      case('0'):
        this.estado = "Llegada Cargue";
        break;
      case('1'):
        this.estado = "Inicia Cargue";
        break;
      case('2'):
        this.estado = "Salida Cargue";
        break;
      }

  }

  //Captura Ubicacion del dispositivo

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {

              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }

  //Metdodo envia novedad segun estado
  setEnviaNovedad(){

    var conexion = this.network.type;

    if(conexion != 'none'){

      var estadoactual = this.parametrosURL.get('estado');

      switch(estadoactual){
        case('0'):
          this.novedad = {
              novedad: 9021,
              checkDate:1,
              descripcionNovedad: "Ok llegada cargue sin Novedad (Registrado desde la APP)"
          };
          this.setData("fec_cumcar");
          this.setRegistraNovedad(this.novedad);
          break;
        case('1'):
          this.novedad = {
              novedad: 9023,
              descripcionNovedad: "Ok inicio cargue sin Novedad (Registrado desde la APP)"
          };
          this.setData("fec_inicar");
          this.setRegistraNovedad(this.novedad);
          break;
        case('2'):
          this.novedad = {
              novedad: 9024,
              descripcionNovedad: "Ok salida cargue sin Novedad (Registrado desde la APP)"
          };
          this.setData("fec_fincar");
          this.setRegistraNovedad(this.novedad);
          break;

        }

    }else{

      /*var estadoactual = this.parametrosURL.get('estado');

      switch(estadoactual){
        case('0'):
          this.novedad = {
              novedad: 9021,
              checkDate:1,
              descripcionNovedad: "Ok llegada cargue sin Novedad (Registrado desde la APP)"
          };
          this.guardaNovedad(this.novedad);
          this.storage.setItem('bloqueaetapa',{bloquea: true});
          break;
        case('1'):
          this.novedad = {
              novedad: 9023,
              descripcionNovedad: "Ok inicio cargue sin Novedad (Registrado desde la APP)"
          };
          this.guardaNovedad(this.novedad);
          this.storage.setItem('bloqueaetapa',{bloquea: true});
          break;
        case('2'):
          this.novedad = {
              novedad: 9024,
              descripcionNovedad: "Ok salida cargue sin Novedad (Registrado desde la APP)"
          };
          this.guardaNovedad(this.novedad);
          this.storage.setItem('bloqueaetapa',{bloquea: true});
          break;

        }*/

        this.app.getNotificacion("Actualemnte no tiene conexión a internet","danger");

    }

  }

  private guardaNovedad(novedad){

    var fecha = moment(new Date()).format("YYYY-MM-DD");
    var hora = moment(new Date()).format("HH:mm");

    var date = fecha+"-"+hora;

    var databd = {vista: "Cargue", contenido: JSON.stringify(novedad), fecha: date, novedad: novedad['novedad'], despacho: JSON.stringify(this.despacho)};

    console.log(databd);

    this.bd.create(databd)
      .then(response => {

        console.log(response);

      })
      .catch( error => {

        console.error( error );

      });


  }

  //Metodo para enviar un Sin novedad
  setMiUbicacion(){

    var conexion = this.network.type;

    if(conexion != 'none'){

      this.novedad = {
        novedad: 9019,
        descripcionNovedad: "Ok Sin Novedad (Registrado desde la APP)"
      };

      this.setRegistraNovedad(this.novedad);

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  setData(opcion){

    var opcionesArray = { op: "setFecAditio", tipoFecha: opcion };

    var datosArray = {

        despacho: this.despacho,
        usuario: this.usuario

    };

      this.opciones = Object.assign(opcionesArray,datosArray);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = JSON.parse(respuesta.json());

        if(dataRespond['cod_respon'] == 1000){

          this.app.getNotificacion("Se ha actualizado el estado", "success");


        }else{

          this.app.getNotificacion("Hubo un error actualizando el estado", "danger");

        }

      });

  }

  setRegistraNovedad(novedad){

      let load = this.loader.create({

        content: 'Registrando...'

      });

      load.present();

      //Crea Fecha
      var f = new Date();
      let dd: number = f.getUTCDate();
      let mm: number = f.getMonth() + 1;

      if(dd < 10){

        var dia = dd.toString();

        dia = "0"+dia;

      }else{

        var dia = dd.toString();

      }

      if(mm < 10){

        var mes = String(mm);

        mes = "0"+mes;

      }

      var fecha = f.getFullYear()+"-"+mes+"-"+dia+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: this.despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion

      };

      this.opciones = Object.assign(opcionesArray,datosArray);

      console.log(this.opciones);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

          load.dismiss();
          this.app.getNotificacion("Se ha registrado la novedad", "success");
          this.navCtrl.setRoot(DespachosPage);

        }else{

          load.dismiss();
          this.app.getNotificacion("Algo salio mal", "alert");
          this.navCtrl.setRoot(DespachosPage);

        }

      }, error => console.log(error));
  }

  setNovedadEspecial(){

      this.navCtrl.push(NovedadespecialPage, {etapa: this.etapa, estado: this.estado, despacho: this.despacho, usuario: this.usuario},{animate: true, direction: 'forward'});

  }


}
