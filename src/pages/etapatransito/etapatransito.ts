import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';
import * as moment from 'moment';

import { DespachosPage } from '../despachos/despachos';
import { NovedadespecialPage } from '../novedadespecial/novedadespecial';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';
import { BdProvider } from '../../providers/bd/bd';

/**
 * Generated class for the Etapatransito page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-etapatransito',
  templateUrl: 'etapatransito.html',
})
export class EtapatransitoPage {

  public estado: any;
  public etapa: any;
  public despacho: any;
  public usuario: any;
  public novedad: any;
  public latitud: any;
  public longitud: any;
  public direccion: any;
  public opciones: any;
  public datosNovedad: any;
  public manifiesto: any;

  constructor(
    public navCtrl: NavController,
    public parametrosURL: NavParams,
    private loader: LoadingController,
    private storage: NativeStorage,
    private geolocation: Geolocation,
    public api: ApiFucntionsProvider ,
    private network: Network,
    private bd: BdProvider,
    public app: AppFunctionsProvider

  ) {

    this.storage.getItem('usuario')
    .then((result) => {

          this.usuario = result;

    }, error => console.log(error));

  }

  ionViewWillEnter(){

      let load = this.loader.create({

        content: 'Cargando...'

      });

      load.present();
      this.etapa = this.parametrosURL.get('etapa');
      this.despacho = this.parametrosURL.get('despacho');
      this.manifiesto = this.parametrosURL.get('manifiesto');
      this.getGeolocalizacion();
      load.dismiss();

  }

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {

              console.log(result);

              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }

  setRegistraNovedad(novedad){

      let load = this.loader.create({

        content: 'Registrando...'

      });

      load.present();

      //Crea Fecha
      var f = new Date();
      let dd: number = f.getUTCDate();
      let mm: number = f.getMonth() + 1;

      var dia = null;

      if(dd < 10){

        dia = dd.toString();

        dia = "0"+dia;

      }else{

        dia = dd.toString();

      }

      if(mm < 10){

        var mes = String(mm);

        mes = "0"+mes;

      }

      var fecha = f.getFullYear()+"-"+mes+"-"+dia+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: this.despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion

      };

      this.opciones = Object.assign(opcionesArray,datosArray);

      console.log(this.opciones);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

          load.dismiss();
          this.app.getNotificacion("Se ha insertado la novedad", "success");
          this.navCtrl.setRoot(DespachosPage);

        }else{

          load.dismiss();
          this.app.getNotificacion("Algo salio mal", "alert");
          this.navCtrl.setRoot(DespachosPage);

        }

      }, error => console.log(error));
  }

  private guardaNovedad(novedad){

    var fecha = moment(new Date()).format("YYYY-MM-DD");
    var hora = moment(new Date()).format("HH:mm");

    var date = fecha+"-"+hora;

    var databd = {vista: "Transito", contenido: JSON.stringify(novedad), fecha: date, novedad: novedad['novedad'], despacho: JSON.stringify(this.despacho)};

    console.log(databd);

    this.bd.create(databd)
      .then(response => {

        console.log(response);

      })
      .catch( error => {

        console.error( error );

      });


  }


  setMiUbicacion(){

    var conexion = this.network.type;

    this.novedad = {
      novedad: 9019,
      descripcionNovedad: "Ok Sin Novedad (Registrado desde la APP)"
    };

    if(conexion != 'none'){

      this.setRegistraNovedad(this.novedad);

    }else{

      /*this.guardaNovedad(this.novedad);
      this.app.getNotificacion("Se ha almacenado la novedad","danger");*/
      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  setNovedadEspecial(){

      this.navCtrl.push(NovedadespecialPage, {etapa: this.etapa, estado: this.estado, despacho: this.despacho, usuario: this.usuario},{animate: true, direction: 'forward'});

  }


}
