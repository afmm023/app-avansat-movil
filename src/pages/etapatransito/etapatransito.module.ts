import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtapatransitoPage } from './etapatransito';

@NgModule({
  declarations: [
    EtapatransitoPage,
  ],
  imports: [
    IonicPageModule.forChild(EtapatransitoPage),
  ],
  exports: [
    EtapatransitoPage
  ]
})
export class EtapatransitoPageModule {}
