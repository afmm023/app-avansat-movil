import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CambiaclavePage } from './cambiaclave';

@NgModule({
  declarations: [
    CambiaclavePage,
  ],
  imports: [
    IonicPageModule.forChild(CambiaclavePage),
  ],
  exports: [
    CambiaclavePage
  ]
})
export class CambiaclavePageModule {}
