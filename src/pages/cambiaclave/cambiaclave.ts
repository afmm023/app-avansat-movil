import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

import { LoginPage } from '../login/login';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/*! \fn: Modulo de Cambiar clave cuando se activa por primera vez
   * \brief: cambia la clave del usuario cuando inicia por primera vez
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/
@IonicPage()
@Component({
  selector: 'page-cambiaclave',
  templateUrl: 'cambiaclave.html',
})
export class CambiaclavePage {

  //Variables generales
  clave: FormGroup;
  public userParametro: any;
  private datos: any;
  public typeN: any;
  public typeC: any;

  constructor(
    public navCtrl: NavController,
    private loader: LoadingController,
    private builder: FormBuilder,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private alertCtrl: AlertController,
    private storage: NativeStorage,
    private network: Network,
    public parametrosURL: NavParams
  ) {

    //Valida que los campos no esten vacios en el formulario
    this.clave = builder.group({
      'clv_anteri': ['', Validators.required],
      'clv_nuevax': ['', Validators.required],
      'clv_nuevaxx': ['', Validators.required],
    });

  }

  ionViewWillEnter(){

      //Carga el tipo de campo en modo password
      this.typeN = "password";
      this.typeC = "password";

  }

  //Cambio el tipo de campo del formulario según la acción
  verPassNueva(){

      if(this.typeN == "password"){

            this.typeN = "text";

      }else{

        this.typeN = "password";

      }

  }

  //Cambio el tipo de campo del formulario según la acción
  verPassConfi(){

      if(this.typeC == "password"){

            this.typeC = "text";

      }else{

        this.typeC = "password";

      }

  }

  /*! \fn: Metodo que recibe los valores enviados desde el formualrio
     * \brief: recibe array con los valores del formulario
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: Array
     * \return ninguno
  */
  onSubmit(datosClave) {

      var conexion = this.network.type;

      if(conexion != 'none'){

        //Crea loader para la vista
        let load = this.loader.create({

          content: 'Verificando...'

        });

        //validamos que la nueva contraseña no se igual a la generada
        if(datosClave['clv_anteri'] == datosClave['clv_nuevax']){

          //Oculta loader
          load.dismiss();

          //muestra popalert
          this.app.getNotificacion("La nueva contraseña no puede ser igual a la generada", "danger");

          //reinici el formulario
          this.clave.reset();

        }else{

          //validamos que la nueva contraseña sea igual a la confirmada
          if(datosClave['clv_nuevax'] == datosClave['clv_nuevaxx']){

            //guardamos usuario enviado desde la anterior vista
            this.userParametro = this.parametrosURL.get("usuario");

            //consultamos la empresa, a la aque el usuario se encuentra relacionado
            this.storage.getItem('empresa')
              .then((result) => {

                var empresa = result;

                var accion = "1";

                //Creamos el array de opcioens que se enviaran a la API
                var opcionesArray = { op: "cambiarClave",empresa: empresa, usuario: this.userParametro, accion: accion};

                //Creamos array con los valores del formulario
                var claveArray = datosClave;

                this.datos = Object.assign(opcionesArray,claveArray);

                load.present();

                //Realiza la petición a la Api
                this.api.getUsuarioControlador(this.datos)
                  .then((respuesta) => {

                      var dataRespond = respuesta.json();

                      if (dataRespond['estado'] == 1){

                        load.dismiss();

                        this.app.getNotificacion("Se ha actualizado con éxito tu contraseña","success");

                        this.storage.setItem('activo',{activo: true});

                        this.navCtrl.setRoot(LoginPage,{},{animate: true,animation: 'md-transition', direction: 'forward'});

                      }else{

                        load.dismiss();

                        this.app.getNotificacion("Algo salio mal, Por favor intentalo de nuevo","danger");

                        this.clave.reset();
                      }

                }, (error) => {

                    console.log(error);

                });

              }, error =>

                console.log(error));

          }
          else{

            load.dismiss(),
            this.app.getNotificacion("La nueva contraseña no coincide", "danger");
            this.clave.reset();


          }

        }

      }else{

          this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

}
