import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AppVersion } from '@ionic-native/app-version';


@IonicPage()
@Component({
  selector: 'page-acercade',
  templateUrl: 'acercade.html',
})
export class AcercadePage {

  public vr: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public version: AppVersion) {

      this.version.getVersionNumber().then((data) => {
          this.vr = data;
      });

  }

}
