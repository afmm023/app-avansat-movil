import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import {Camera, CameraOptions} from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';
import * as moment from 'moment';

import { DespachosPage } from '../despachos/despachos';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';
import { BdProvider } from '../../providers/bd/bd';


@IonicPage()
@Component({
 selector: 'page-enviafacturas',
 templateUrl: 'enviafacturas.html',
})
export class EnviafacturasPage {

   formfactura: FormGroup;
   public despacho: any;
   public usuario: any;
   public novedad: any;
   public documento: any;
   public datosNovedad: any;
   public opciones: any;
   public photos : any;
   public temporal: any;
   public base64Image : string;
   public latitud: any;
   public longitud: any;
   public direccion: any;
   public manifiesto: any;

 constructor(

   public navCtrl: NavController,
   private builder: FormBuilder,
   public api: ApiFucntionsProvider,
   private loader: LoadingController,
   public parametrosURL: NavParams,
   public app: AppFunctionsProvider,
   private camera : Camera,
   private network: Network,
   public alertCtrl: AlertController,
   private bd: BdProvider,
   private geolocation: Geolocation

 ) {

   this.formfactura = builder.group({
     'observacion': ['', Validators.required],
   });


 }

 ionViewWillEnter(){

   let load = this.loader.create({

     content: 'Cargando...'

   });

   load.present();
   this.despacho = this.parametrosURL.get('despacho');
   this.usuario = this.parametrosURL.get('usuario');
   //this.facturas = this.parametrosURL.get('factura');
   this.documento = this.parametrosURL.get('documento');
   this.manifiesto = this.parametrosURL.get('manifiesto');
   this.photos = [];
   this.getGeolocalizacion();
   load.dismiss();

   console.log(this.despacho+"----"+this.usuario+"--------"+this.documento+"-------"+this.manifiesto+"---------");

 }

 deletePhoto(index) {
    let confirm = this.alertCtrl.create({
        title: 'Aviso',
        message: 'Desea eliminar la foto',
        buttons: [
          {
            text: 'No',
            handler: () => {
              console.log('Disagree clicked');
            }
          }, {
            text: 'SI',
            handler: () => {
              this.photos.splice(index, 1);
            }
          }
        ]
      });
    confirm.present();
  }

  takePhoto() {
    const options : CameraOptions = {
      quality: 30, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options) .then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();
      }, (err) => {
        console.log(err);
      });
  }

  onSubmit(values){

      var conexion = this.network.type;

      this.novedad = {
          novedad: 9020,
          observacion: values['observacion'],
          tip_noveda: 2,
          dataCumplidos: this.photos,
          documento: this.documento,
          etapa: "cumplidos"
      };

      if(conexion != 'none'){

          if(this.photos['length'] == 0){

            this.app.getNotificacion("Debe capturar mínimo una foto","danger");

          }else{

            this.setRegistraNovedad(this.novedad);

          }

      }else{

        /*this.guardaNovedad(this.novedad);
        this.app.getNotificacion("Se ha almacenado la novedad","danger");*/
        this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

  private guardaNovedad(novedad){

    var fecha = moment(new Date()).format("YYYY-MM-DD");
    var hora = moment(new Date()).format("HH:mm");

    var date = fecha+"-"+hora;

    var databd = {vista: "Cumplidos", contenido: JSON.stringify(novedad), fecha: date, novedad: novedad['novedad'], despacho: JSON.stringify(this.despacho)};

    console.log(databd);

    this.bd.create(databd)
      .then(response => {

        console.log(response);

      })
      .catch( error => {

        console.error( error );

      });


  }

  setRegistraNovedad(novedad){

      let load = this.loader.create({

        content: 'Registrando...'

      });

      load.present();

      //Crea Fecha
      var f = new Date();
      let dd: number = f.getUTCDate();
      let mm: number = f.getMonth() + 1;

      var dia = "";

      if(dd < 10){

        dia = dd.toString();

        dia = "0"+dia;

      }else{

        dia = dd.toString();

      }

      if(mm < 10){

        var mes = String(mm);

        mes = "0"+mes;

      }

      var fecha = f.getFullYear()+"-"+mes+"-"+dia+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: this.despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion

      };

      this.opciones = Object.assign(opcionesArray,datosArray);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

            load.dismiss();
            this.app.getNotificacion("Se ha insertado la novedad", "success");
            this.navCtrl.setRoot(DespachosPage);

        }else{

            load.dismiss();
            this.app.getNotificacion("Algo salio mal", "alert");
            this.navCtrl.setRoot(DespachosPage);

        }

      }, error => console.log(error));
  }

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {

              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }


}
