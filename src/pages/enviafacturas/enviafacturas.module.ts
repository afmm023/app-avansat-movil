import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EnviafacturasPage } from './enviafacturas';

@NgModule({
  declarations: [
    EnviafacturasPage,
  ],
  imports: [
    IonicPageModule.forChild(EnviafacturasPage),
  ],
  exports: [
    EnviafacturasPage
  ]
})
export class EnviafacturasPageModule {}
