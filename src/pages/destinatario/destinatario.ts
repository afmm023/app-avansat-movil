import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';

import { DespachosPage } from '../despachos/despachos';
import { NovedadespecialPage } from '../novedadespecial/novedadespecial';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/**
 * Generated class for the Destinatario page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-destinatario',
  templateUrl: 'destinatario.html',
})
export class DestinatarioPage {

  public etapa: any;
  public estado: any;
  public opcion: any;
  public despacho: any;
  public usuario: any;
  public novedad: any;
  public latitud: any;
  public longitud: any;
  public direccion: any;
  public opciones: any;
  public datosNovedad: any;
  public facturas: any;
  public estadodes: any;
  public estadoini: any;
  public estadofin: any;
  public manifiesto: any;

  constructor(
    public navCtrl: NavController,
    public parametrosURL: NavParams,
    private loader: LoadingController,
    private storage: NativeStorage,
    private geolocation: Geolocation,
    public api: ApiFucntionsProvider,
    private network: Network,
    public app: AppFunctionsProvider

  ) {

    this.storage.getItem('usuario')
    .then((result) => {

          this.usuario = result;

    }, error => console.log(error));

  }

  ionViewWillEnter(){

      var conexion = this.network.type;

      if(conexion != 'none'){

        let load = this.loader.create({

          content: 'Cargando...'

        });

        load.present();
        this.etapa = this.parametrosURL.get('etapa');
        this.facturas = this.parametrosURL.get('facturas');
        this.despacho = this.parametrosURL.get('despacho');
        this.estadodes = this.parametrosURL.get('estadodes');
        this.estadoini = this.parametrosURL.get('estadoini');
        this.estadofin = this.parametrosURL.get('estadofin');
        this.manifiesto = this.parametrosURL.get('manifiesto');
        this.getGeolocalizacion();
        this.verificaEstado(this.facturas, this.estadodes, this.estadoini, this.estadofin);
        load.dismiss();

      }else{

        this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

  verificaEstado(facturas,estadodes,estadoini,estadofin){

      var remplazo = /,/gi;

      var factura = "'" + facturas.replace(remplazo , "','") + "'";

      this.facturas = factura;

      if(estadodes == 1){

        var opcion = "fec_cumdes";

        this.opcion = opcion;

        this.estado = "Llegada Descargue";

      }else if(estadoini == 1){

        var opcion = "fec_inides";

        this.opcion = opcion;

        this.estado = "Inicia Descargue";

      }else if(estadofin == 1){

        var opcion = "fec_findes";

        this.opcion = opcion;

        this.estado = "Finaliza Descargue";


      }else {

          this.app.getNotificacion("Se encuentra cumplido", "orange");

      }

  }

  setMiUbicacion(){

    this.novedad = {
      novedad: 9019,
      descripcionNovedad: "Ok Sin Novedad (Registrado desde la APP)"
    };
    this.setRegistraNovedad(this.novedad);

  }

  setEnviaNovedad(){

      this.setData(this.facturas, this.opcion);

  }

  setData(factura, opcion){

    var opcionesArray = { op: "setFecAditio", tipoFecha: opcion, facturas: factura };

    var datosArray = {

        despacho: this.despacho,
        usuario: this.usuario

    };

      this.opciones = Object.assign(opcionesArray,datosArray);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = JSON.parse(respuesta.json());

        if(dataRespond['cod_respon'] == 1000){

          let mensaje = dataRespond['msg_respon'].split('.');

          if(mensaje.length > 1){

              var dataNovedad = {

               novedad: mensaje[1],
               descripcionNovedad: "Ok Sin Novedad (Registrado desde la APP)"

              };

            this.setRegistraNovedad(dataNovedad);

          }

          this.app.getNotificacion("Se ha actualizado el estado", "success");

        }else{

          this.app.getNotificacion("Hubo un error actualizando el estado", "danger");

        }

      });

  }

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {

              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }

  setRegistraNovedad(novedad){

      let load = this.loader.create({

        content: 'Registrando...'

      });

      load.present();

      //Crea Fecha
      var f = new Date();
      let dd: number = f.getUTCDate();
      let mm: number = f.getMonth() + 1;

      if(dd < 10){

        var dia = dd.toString();

        dia = "0"+dia;

      }else{

        var dia = dd.toString();

      }

      if(mm < 10){

        var mes = String(mm);

        mes = "0"+mes;

      }

      var fecha = f.getFullYear()+"-"+mes+"-"+dia+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: this.despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion

      };

      this.opciones = Object.assign(opcionesArray,datosArray);

      console.log(this.opciones);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

          load.dismiss();
          this.app.getNotificacion("Se ha insertado la novedad", "success");
          this.navCtrl.setRoot(DespachosPage);

        }else{

          load.dismiss();
          this.app.getNotificacion("Algo salio mal", "alert");
          this.navCtrl.setRoot(DespachosPage);

        }

      }, error => console.log(error));
  }

  setNovedadEspecial(){

      this.navCtrl.push(NovedadespecialPage, {etapa: this.etapa, estado: this.estado, despacho: this.despacho, usuario: this.usuario},{animate: true, direction: 'forward'});

  }

}
