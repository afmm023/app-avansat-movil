import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DestinatarioPage } from './destinatario';

@NgModule({
  declarations: [
    DestinatarioPage,
  ],
  imports: [
    IonicPageModule.forChild(DestinatarioPage),
  ],
  exports: [
    DestinatarioPage
  ]
})
export class DestinatarioPageModule {}
