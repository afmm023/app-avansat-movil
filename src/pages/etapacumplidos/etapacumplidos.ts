import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams} from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

//import { FacturasPage } from '../facturas/facturas';
import { EnviafacturasPage } from '../enviafacturas/enviafacturas';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';


@IonicPage()
@Component({
  selector: 'page-etapacumplidos',
  templateUrl: 'etapacumplidos.html',
})
export class EtapacumplidosPage {

  public despacho: any;
  public estado: any;
  public etapa: any;
  public dataApi: any;
  public usuario: any;
  public manifiesto: any;
  opcionDestinatario = [];

  constructor(
    public navCtrl: NavController,
    private loader: LoadingController,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private network: Network,
    public parametrosURL: NavParams,
    private alertCtrl: AlertController,
    private storage: NativeStorage
  ) {
  }

  ionViewWillEnter(){

    var conexion = this.network.type;

    if(conexion != 'none'){

      this.storage.getItem('usuario')
      .then((data) => {

          this.usuario = data;

      }, error => console.log(error));
      let load = this.loader.create({

        content: 'Cargando...'

      });

      load.present();
      this.despacho = this.parametrosURL.get('despacho');
      console.log(this.despacho);
      console.log(this.usuario);
      this.estado = this.parametrosURL.get('estado');
      this.etapa = this.parametrosURL.get('etapa');
      this.manifiesto = this.parametrosURL.get('manifiesto');
      this.listarDestinatarios(this.despacho);
      load.dismiss();

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  listarDestinatarios(despacho){


    this.storage.getItem('usuario')
    .then((result) => {

      var opcionesArray = { op: "getDestinatarios", source: "app" , despacho: despacho, usuario: result , etapa: 4};

      this.dataApi = opcionesArray;

        this.api.getDespachoControlador(this.dataApi)
         .then((respuesta) => {

          let dataRespond = respuesta.json();

            this.opcionDestinatario.length = 0;

            for(var i = 0; i <  dataRespond['dat_respon'].length ; i++){

                this.opcionDestinatario.push(dataRespond['dat_respon'][i]);

            }


         }, (error)=>{

           console.log(error);

         });

    },error => console.log(error));


  }

  setFactura(documento){

      //se cambia vista por ajuste solicitado ahora envia el documento del destinatario 
      this.navCtrl.push(EnviafacturasPage, {documento: documento, usuario: this.usuario, despacho: this.despacho, manifiesto: this.manifiesto});

  }

}
