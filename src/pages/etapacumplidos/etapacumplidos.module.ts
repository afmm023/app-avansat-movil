import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtapacumplidosPage } from './etapacumplidos';

@NgModule({
  declarations: [
    EtapacumplidosPage,
  ],
  imports: [
    IonicPageModule.forChild(EtapacumplidosPage),
  ],
  exports: [
    EtapacumplidosPage
  ]
})
export class EtapacumplidosPageModule {}
