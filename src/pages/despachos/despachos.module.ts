import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DespachosPage } from './despachos';

@NgModule({
  declarations: [
    DespachosPage,
  ],
  imports: [
    IonicPageModule.forChild(DespachosPage),
  ],
  exports: [
    DespachosPage
  ]
})
export class DespachosPageModule {}
