import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { EtapadespachoPage } from '../etapadespacho/etapadespacho';
import { TabsPage } from '../tabs/tabs';
import { PernotacionPage } from '../pernotacion/pernotacion';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/*! \fn: Modulo de lista de despachos pendientes
   * \brief: lista todos los despachos pendientes por cumplir
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/
@IonicPage()
@Component({
  selector: 'page-despachos',
  templateUrl: 'despachos.html',
})
export class DespachosPage {

  public dataApi: any;
  public etapa: any;
  public estado: any;
  public fecha: any;
  public despacho: any;
  public despachoData: any;
  opcionDespachos = [];

  constructor(

    public navCtrl: NavController,
    private loader: LoadingController,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private network: Network,
    private alertCtrl: AlertController,
    private storage: NativeStorage

  ) {
  }

  ionViewWillEnter(){

    var conexion = this.network.type;

    if(conexion != 'none'){

      //Crea loader para la vista
      let load = this.loader.create({

        content: 'Cargando...'

      });

      load.present();
      this.listarDespacho();
      load.dismiss();

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  //Cambia a la vista de pernotacion
  pernotacion(){

      this.navCtrl.push( PernotacionPage,{ despacho: this.despachoData },{animate: true, direction: 'forward'} );

  }

  /*! \fn: metodo que  lista los despachos pendientes
     * \brief: lista todos los despachos pendientes por cumplir
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: ninguno
     * \return ninguno
  */
  listarDespacho(){

    this.storage.getItem('pernotando').then(result => {

        if(result['pernotando'] == true){

            this.app.getMostrarAlerta("Aviso", "Debe reiniciar su ruta actual","Aceptar");

            //cambia a la vista de pernotacion
            this.navCtrl.setRoot(PernotacionPage,{},{ animate: true, direction: 'forward'});

        }else {

          var opcionesArray = { op: "listarDespachos", source: "app"};

          this.storage.getItem('usuario')
          .then((result) => {

            var userArray = result;

            this.dataApi = Object.assign(opcionesArray,userArray);

              this.api.getDespachoControlador(this.dataApi)
               .then((respuesta) => {

                var dataRespond = respuesta.json();

                this.opcionDespachos.length = 0;

                if(dataRespond['cod_respon'] == 1000 ){

                  this.despachoData = dataRespond;

                  console.log(dataRespond);

                  for(var i = 0; i < ((Object.keys(dataRespond).length) - 1 ); i++){

                      let posicion = dataRespond[i];
                      this.opcionDespachos.push(posicion);

                  }

                }else{

                  this.app.getNotificacion("No tiene manifiestos disponibles","danger");
                  this.navCtrl.setRoot(TabsPage);

                }

               });

          },error => console.log(error));


        }
    }, error => {

        var opcionesArray = { op: "listarDespachos", source: "app"};

        this.storage.getItem('usuario')
        .then((result) => {

          var userArray = result;

          this.dataApi = Object.assign(opcionesArray,userArray);

            this.api.getDespachoControlador(this.dataApi)
             .then((respuesta) => {

              var dataRespond = respuesta.json();

              this.opcionDespachos.length = 0;

              if(dataRespond['cod_respon'] == 1000 ){

                this.despachoData = dataRespond;

                console.log(dataRespond);

                for(var i = 0; i < ((Object.keys(dataRespond).length) - 1 ); i++){

                    let posicion = dataRespond[i];
                    this.opcionDespachos.push(posicion);

                }

              }else{

                this.app.getNotificacion("No tiene manifiestos disponibles","danger");
                this.navCtrl.setRoot(TabsPage);

              }

             });

        },error => console.log(error));

    });
  }

  /*! \fn: metodo que  captura la acction de la lista
     * \brief: captura accion de la lista
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: ninguno
     * \return ninguno
  */
  setDespacho(despachoid, manifiesto){

    this.navCtrl.push(EtapadespachoPage,{despacho: despachoid, manifiesto: manifiesto},{ animate: true, direction: 'forward'});

  }


}
