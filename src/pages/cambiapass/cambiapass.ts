import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';

import { LoginPage } from '../login/login';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/*! \fn: Modulo de Cambio de clave por medio de la vista de opciones
   * \brief: cambia la contraseña del usuario por medio del menu de opciones
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/
@IonicPage()
@Component({
  selector: 'page-cambiapass',
  templateUrl: 'cambiapass.html',
})
export class CambiapassPage {

  //variables generales
  clave: FormGroup;
  public datos: any;
  public typeN: any;
  public typeC: any;

  constructor(
    public navCtrl: NavController,
    private loader: LoadingController,
    private builder: FormBuilder,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private alerta: AlertController,
    public parametrosURL: NavParams,
    public appm: App,
    private network: Network,
    private device: Device
  ) {

    //valida que los campos del formulario no se encuentren vacios
    this.clave = builder.group({
      'clv_nuevax': ['', Validators.required],
      'clv_nuevaxx': ['', Validators.required],
      'documento': ['', Validators.required],
    });

  }

  ionViewWillEnter(){

    //Carga el tipo de campo en modo password
      this.typeN = "password";
      this.typeC = "password";

  }

  //Cambio el tipo de campo del formulario según la acción
  verPass2(){

      if(this.typeN == "password"){

            this.typeN = "text";

      }else{

        this.typeN = "password";

      }

  }

  //Cambio el tipo de campo del formulario según la acción
  verPass3(){

      if(this.typeC == "password"){

            this.typeC = "text";

      }else{

        this.typeC = "password";

      }

  }

  /*! \fn: Metodo que recibe los valores enviados desde el formualrio
     * \brief: recibe array con los valores del formulario
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: Array
     * \return ninguno
  */
  onSubmit(datosClave){

      var conexion = this.network.type;

      if(conexion != 'none'){

        let confirm = this.alerta.create({
              title: 'Aviso',
              message: '¿Realmente quieres actualizar tu contraseña?',
              buttons: [
                {
                  text: 'Cancelar',
                  handler: () => {

                    this.clave.reset();

                  }
                },
                {
                  text: 'Actualizar',
                  handler: () => {

                        //Crea loader para la vista
                        let load = this.loader.create({

                          content: 'Verificando...'

                        });

                        //validamos que la nueva contraseña sea igual a la confirmada
                        if(datosClave['clv_nuevax'] == datosClave['clv_nuevaxx']){

                          //Creamos el array de opcioens que se enviaran a la API
                          var opcionesArray = {op: "ActualizaPass",cod_uuid: this.device.uuid };

                          //Creamos array con los valores del formulario
                          var claveArray = datosClave;

                          this.datos = Object.assign(opcionesArray,claveArray);

                          load.present();

                          //Realiza la petición a la Api
                          this.api.getUsuarioControlador(this.datos)
                            .then((respuesta) => {

                                var dataRespond = respuesta.json();

                                if (dataRespond['estado'] == 1){

                                  load.dismiss();

                                  this.app.getNotificacion("Se ha actualizado con éxito tu contraseña","success");

                                  this.appm.getRootNav().setRoot(LoginPage);

                                }else{

                                  load.dismiss();

                                  this.app.getNotificacion("Algo salio mal, Por favor intentalo de nuevo","danger");

                                  this.clave.reset();
                                }

                          });

                        }else{

                          load.dismiss(),
                          this.app.getNotificacion("La nueva contraseña no es igual", "danger");
                          this.clave.reset();

                        }

                  }
                }
              ]
            });
            confirm.present();

      }else{

          this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }


  }

}
