import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CambiapassPage } from './cambiapass';

@NgModule({
  declarations: [
    CambiapassPage,
  ],
  imports: [
    IonicPageModule.forChild(CambiapassPage),
  ],
  exports: [
    CambiapassPage
  ]
})
export class CambiapassPageModule {}
