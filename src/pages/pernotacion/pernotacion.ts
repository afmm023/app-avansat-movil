import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';

import * as moment from 'moment';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { DatePicker } from '@ionic-native/date-picker';


import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';
import { BdProvider } from '../../providers/bd/bd';


@IonicPage()
@Component({
  selector: 'page-pernotacion',
  templateUrl: 'pernotacion.html',
})
export class PernotacionPage {


  //Formulario y datos para programacion de ruta
  pernotar: FormGroup;
  fecha: any;
  estado: any;
  hoy: any;
  hora: any;
  minutos: number;

  //Datos Novedad
  public latitud: any;
  public longitud: any;
  public direccion: any;
  public novedad: any;
  public despacho: any;
  public usuario: any;
  public datosNovedad: any;
  public opciones: any;
  public fechahora: any

  //Estados de los estilo de la vista
  public nombrepernotando: any;
  public observacionpernotando: any;
  public reiniciopernotando: any;


  constructor(

    public navCtrl: NavController,
    public parametrosURL: NavParams,
    private builder: FormBuilder,
    private storage: NativeStorage,
    private geolocation: Geolocation,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private datePicker: DatePicker,
    private network: Network,
    private bd: BdProvider
    //private localNotifications: LocalNotifications

  ) {


    this.storage.getItem('usuario')
    .then((result) => {

          this.usuario = result;

    }, error => console.log(error));

    this.pernotar = this.builder.group({
      'parqueadero': ['', Validators.required],
      'observacion': ['', Validators.required]

    });

    this.fechahora = "--";


  }

  ionViewWillEnter() {


    this.estadoPernotacion();
    this.getGeolocalizacion();
    this.hoy = moment(new Date()).format("YYYY-MM-DD");
    this.fecha = this.hoy;
    this.hora = moment(new Date()).format("HH:mm");
    this.despacho = this.parametrosURL.get('despacho');

  }

  estadoPernotacion(){

      this.storage.getItem('pernotando')
      .then((result) => {

          if (result['pernotando'] == true){

              this.nombrepernotando = true;
              this.observacionpernotando = true;
              this.reiniciopernotando = false;


          }else{

            this.nombrepernotando = false;
            this.observacionpernotando = false;
            this.reiniciopernotando = true;


          }

      }, error => {

        this.nombrepernotando = false;
        this.observacionpernotando = false;
        this.reiniciopernotando = true;

      });

  }

  reincioRuta(){

    this.storage.getItem('despachos').then(data => {

      this.despacho = data['despachos'];

      this.novedad = {
        novedad: 9025,
        descripcionNovedad: "Conductor reporta reinicio de ruta (Registrado desde la APP)"
      };

      var condicion = (Object.keys(this.despacho).length) - 1;

      console.log(this.despacho);

      console.log(condicion);

      for(var i = 0; i < condicion ; i++){

            var despacho = this.despacho[i];

            this.setRegistraNovedad(this.novedad, despacho);

      }

      this.storage.setItem('pernotando', {pernotando: false});
      this.estado = "true";
      this.app.getNotificacion("Se ha reiniciado su ruta","alert");

      this.navCtrl.setRoot(TabsPage,{},{animate: true, direction: 'forward'});

    }, error => console.log("no hay nada"));

  }

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {

              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }

  getFecha(){

    var options = {

      date: new Date(),
      mode: 'datetime',
      titleText: 'Fecha de reinicio de ruta',
      okText: 'Aceptar',
      cancelText: 'Cancelar',
      allowOldDates: true,
      locale: 'es_CO',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK

    };

    this.datePicker.show(options)
    .then(date => {

      var f = moment(date).format('YYYY-MM-DD');
      var h = moment(date).format('HH:mm');

      this.fechahora = f+" "+h;


    },error => console.log(error));

  }


  onSubmit(values){

    var conexion = this.network.type;

    var observacion = "Parqueadero: "+values['parqueadero']+" - "+values['observacion'] ;

    if(conexion != 'none'){

      if(this.fechahora == "--"){

          this.app.getNotificacion("Debe seleccionar la fecha de reinicio de ruta","alert");

      }else{

        this.storage.setItem('despachos',{despachos: this.despacho});

        this.novedad = {
          novedad: 9008,
          descripcionNovedad: "Conductor reporta Pernotación (Registrado desde la APP)",
          observacion: observacion,
          fechaRenicio: this.fechahora
        };

        var condicion = (Object.keys(this.despacho).length) - 1;

        for(var i = 0; i < condicion; i++){

              var despachox = this.despacho[i];

              this.setRegistraNovedad(this.novedad, despachox);

        }

        this.app.getNotificacion("Se ha programado el reinicio de ruta","alert");

        this.storage.setItem('pernotando', {pernotando: true});

        this.navCtrl.setRoot(TabsPage,{},{animate: true, direction: 'forward'});

      }

    }else{



      this.novedad = {
        novedad: 9008,
        descripcionNovedad: "Conductor reporta Pernotación (Registrado desde la APP)",
        observacion: observacion,
        fechaReinicio: this.fechahora
      };

      /*this.guardaNovedad(this.novedad);
      this.app.getNotificacion("Se ha almacenado la novedad","danger");*/
      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  private guardaNovedad(novedad){

    var fecha = moment(new Date()).format("YYYY-MM-DD");
    var hora = moment(new Date()).format("HH:mm");

    var date = fecha+"-"+hora;

    var databd = {vista: "Pernoctando", contenido: JSON.stringify(novedad), fecha: date, novedad: novedad['novedad'], despacho: JSON.stringify(this.despacho)};

    console.log(databd);

    this.bd.create(databd)
      .then(response => {

        console.log(response);

      })
      .catch( error => {

        console.error( error );

      });


  }


  setRegistraNovedad(novedad, despacho){

      var f = moment(new Date()).format('YYYY-MM-DD');
      var h = moment(new Date()).format('HH:mm');

      var fecha = f+" "+h;
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion
      };

      this.opciones = Object.assign(opcionesArray,datosArray);


      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

          this.app.getNotificacion("Se ha reportado la novedad","success");

        }else{

          this.app.getNotificacion("Hubo un error al reportar la novedad","danger");

        }

      }, error => console.log(error));
  }


}
