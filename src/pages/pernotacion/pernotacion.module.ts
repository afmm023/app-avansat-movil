import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PernotacionPage } from './pernotacion';

@NgModule({
  declarations: [
    PernotacionPage,
  ],
  imports: [
    IonicPageModule.forChild(PernotacionPage),
  ],
  exports: [
    PernotacionPage
  ]
})
export class PernotacionPageModule {}
