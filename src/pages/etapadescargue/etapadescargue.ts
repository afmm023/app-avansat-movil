import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams} from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';

import { DestinatarioPage } from '../destinatario/destinatario';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/**
 * Generated class for the Etapadescargue page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-etapadescargue',
  templateUrl: 'etapadescargue.html',
})
export class EtapadescarguePage {

  public despacho: any;
  public estado: any;
  public etapa: any;
  public dataApi: any;
  public usuario: any;
  public manifiesto: any;
  opcionDestinatario = [];

  constructor(
    public navCtrl: NavController,
    public api: ApiFucntionsProvider,
    private loader: LoadingController,
    public app: AppFunctionsProvider,
    private network: Network,
    public parametrosURL: NavParams,
    private alertCtrl: AlertController,
    private storage: NativeStorage
  ) {
  }

  ionViewWillEnter(){

      var conexion = this.network.type;

      if(conexion != 'none'){

        this.storage.getItem('usuario')
        .then((data) => {

            this.usuario = data;

        }, error => console.log(error));
        let load = this.loader.create({

          content: 'Cargando...'

        });

        load.present();
        this.despacho = this.parametrosURL.get('despacho');
        this.estado = this.parametrosURL.get('estado');
        this.etapa = this.parametrosURL.get('etapa');
        this.manifiesto = this.parametrosURL.get('manifiesto');
        this.listarDestinatarios(this.despacho);
        load.dismiss();

      }else{

        this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

  listarDestinatarios(despacho){


    this.storage.getItem('usuario')
    .then((result) => {

      var opcionesArray = { op: "getDestinatarios", source: "app" , despacho: despacho, usuario: result};

      this.dataApi = opcionesArray;

        this.api.getDespachoControlador(this.dataApi)
         .then((respuesta) => {

          let dataRespond = respuesta.json();

            this.opcionDestinatario.length = 0;

            for(var i = 0; i <  dataRespond['dat_respon'].length ; i++){

              var valida = dataRespond['dat_respon'][i];

              var numero = valida['ind_findes'];

              var final= +numero;

              if(final > 0){

                  this.opcionDestinatario.push(dataRespond['dat_respon'][i]);

              }

            }


         }, (error)=>{

           console.log(error);

         });

    },error => console.log(error));


  }

  setNovedad(facturas,estadodes,estadoini,estadofin){

      this.navCtrl.push(DestinatarioPage, {facturas: facturas, estadodes: estadodes, estadoini: estadoini, estadofin: estadofin, usuario: this.usuario, despacho: this.despacho, etapa: this.etapa, manifiesto: this.manifiesto});

  }


}
