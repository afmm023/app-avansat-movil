import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtapadescarguePage } from './etapadescargue';

@NgModule({
  declarations: [
    EtapadescarguePage,
  ],
  imports: [
    IonicPageModule.forChild(EtapadescarguePage),
  ],
  exports: [
    EtapadescarguePage
  ]
})
export class EtapadescarguePageModule {}
