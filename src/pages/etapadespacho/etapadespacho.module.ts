import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EtapadespachoPage } from './etapadespacho';

@NgModule({
  declarations: [
    EtapadespachoPage,
  ],
  imports: [
    IonicPageModule.forChild(EtapadespachoPage),
  ],
  exports: [
    EtapadespachoPage
  ]
})
export class EtapadespachoPageModule {}
