import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { EtapacarguePage } from '../etapacargue/etapacargue';
import { EtapatransitoPage } from '../etapatransito/etapatransito';
import { EtapadescarguePage } from '../etapadescargue/etapadescargue';
import { EtapacumplidosPage } from '../etapacumplidos/etapacumplidos';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';


@IonicPage()
@Component({
  selector: 'page-etapadespacho',
  templateUrl: 'etapadespacho.html',
})
export class EtapadespachoPage {

  public despacho: any;
  public despachoData: any;
  public manifiesto: any;
  public etapa: any;
  public estado: any;
  public classcargue : any;
  public classtransito : any;
  public classdescargue : any;
  public classcumplido : any;
  public buttoncargue: any;
  public buttontransito: any;
  public buttondescargue: any;
  public buttoncumplido: any;
  public dataApi: any;


  constructor(

    public navCtrl: NavController,
    public parametrosURL: NavParams,
    private loader: LoadingController,
    public api: ApiFucntionsProvider ,
    public app: AppFunctionsProvider,
    private network: Network,
    private storage: NativeStorage

    ) {
  }

  ionViewWillEnter() {

      var conexion = this.network.type;

      if( conexion != 'none'){

        let load = this.loader.create({

          content: 'Cargando...'

        });

        load.present();
        //this.consultaPendientes();
        this.despacho = this.parametrosURL.get('despacho');
        this.manifiesto = this.parametrosURL.get('manifiesto');
        this.consultaDespacho(this.despacho);
        load.dismiss();

      }else{

        this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

  novedadCargue(){

      this.navCtrl.push( EtapacarguePage ,{estado: this.estado, despacho: this.despachoData, etapa: this.etapa, despachoid: this.despacho, manifiesto: this.manifiesto},{animate: true, direction: 'forward'} )

  }

  novedadTransito(){

      this.navCtrl.push( EtapatransitoPage ,{estado: this.estado, despacho: this.despachoData, etapa: 3, manifiesto: this.manifiesto},{animate: true, direction: 'forward'} )

  }

  novedadDescargue(){

      this.navCtrl.push(EtapadescarguePage, {estado: this.estado, despacho: this.despachoData, etapa: 4, despachoid: this.despacho, manifiesto: this.manifiesto},{animate: true, direction: 'forward'});

  }

  novedadCumplido(){

      this.navCtrl.push(EtapacumplidosPage, {estado: this.estado, despacho: this.despachoData, etapa: 5, despachoid: this.despacho, manifiesto: this.manifiesto},{animate: true, direction: 'forward'});

  }


  consultaDespacho(despachoid){

    this.storage.getItem('usuario')
    .then((result) => {

      var userArray = result;

      var opcionesArray = { op: "consultarDespacho", source: "app", despacho: despachoid };

      this.dataApi = Object.assign(opcionesArray,userArray);

      this.api.getDespachoControlador(this.dataApi)
       .then((respuesta) => {

          var dataRespond = respuesta.json();

          if(dataRespond['cod_respon'] == 1000 ){

            this.despachoData = dataRespond;
            this.estado = dataRespond['loadState'];
            this.etapa = dataRespond['etapa'];

            this.etapaActual(this.etapa);

          }else{

            this.app.getMostrarAlerta("Aviso","El conductor no tiene despachos asociados","Aceptar");

          }

       }, (error)=>{

         console.log(error);

       });

    }, error => console.log(error));


  }

  consultaPendientes(){

    this.storage.getItem('bloqueaetapa')
    .then((resultado) => {

        if(resultado['bloquea'] == true){

          this.classcargue = "img-responsive-gris";
          this.classtransito = "img-responsive-gris";
          this.classdescargue = "img-responsive-gris";
          this.classcumplido = "img-responsive-gris";

          this.buttoncargue = true;
          this.buttontransito = true;
          this.buttondescargue = true;
          this.buttoncumplido = true;

          this.app.getMostrarAlerta("Aviso","Actualmente tiene novedades pendientes por enviar, por favor conectese a una red de internet y actualize su estado","Aceptar");

        }

    }, error => console.log("no hay variable"));


  }

  etapaActual(etapa){

    switch (etapa){
      case'2':
        this.classcargue = "img-responsive";
        this.classtransito = "img-responsive-gris";
        this.classdescargue = "img-responsive-gris";
        this.classcumplido = "img-responsive-gris";

        this.buttoncargue = false;
        this.buttontransito = true;
        this.buttondescargue = true;
        this.buttoncumplido = true;

      break;
      case '3':
        this.classcargue = "img-responsive-gris";
        this.classtransito = "img-responsive";
        this.classdescargue = "img-responsive";
        this.classcumplido = "img-responsive";

        this.buttoncargue = true;
        this.buttontransito = false;
        this.buttondescargue = false;
        this.buttoncumplido = false;

      break;
      case '4':
        this.classcargue = "img-responsive-gris";
        this.classtransito = "img-responsive";
        this.classdescargue = "img-responsive";
        this.classcumplido = "img-responsive";

        this.buttoncargue = true;
        this.buttontransito = false;
        this.buttondescargue = false;
        this.buttoncumplido = false;

      break;
    }


  }

}
