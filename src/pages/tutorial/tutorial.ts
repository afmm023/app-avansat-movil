import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ActivacionPage } from '../activacion/activacion';

/**
 * Generated class for the Tutorial page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  setActivate() {

    this.navCtrl.setRoot(ActivacionPage, null, {animate: true,animation: 'md-transition', direction: 'forward'});
  }

}
