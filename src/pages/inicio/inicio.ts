import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';
import { OneSignal } from '@ionic-native/onesignal';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';
import { BdProvider } from '../../providers/bd/bd';

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
})
export class InicioPage {

  public usuario: any;
  public vehiculo: any;
  public token: any;
  public push : any;

  constructor(

    public navCtrl: NavController,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private storage: NativeStorage,
    private network: Network,
    private device: Device,
    private oneSignal: OneSignal,
    private bd: BdProvider,
    public parametrosURL: NavParams

  ) {

  }

  ionViewWillEnter(){

      //Configuración de OneSignal
      var idonesignal = 'd69188cb-f405-4281-b33a-6926810afa55';
      var idfirebase = '445629727083';

      //Realiza conexión a Onesignal
      this.oneSignal.startInit(idonesignal, idfirebase);

      //Trae el Id y el push token del dispositivo
      this.oneSignal.getIds()
        .then(

          (result: any) => {

            this.validaToken(result.userId, result.pushToken);

          }

        ).catch((error: any) => console.log(error));

      this.oneSignal.enableVibrate(true);

      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

      this.oneSignal.endInit();

      var parametros = this.parametrosURL.get('data');

      if(parametros){

            this.consultaConductor(parametros);

      }else{

        this.storage.getItem('usuario').then((result) => {

        var data = result;

          this.consultaConductor(data);
          this.getAllTasks();

        });

    }

  }

  getAllTasks(){
    this.bd.getAll()
    .then(tasks => {

      console.log(tasks);

    })
    .catch( error => {

      console.error( error );

    });
  }

  consultaConductor(usuariodata){

    var conexion = this.network.type;

    if (conexion != 'none'){

      var userdata = usuariodata;

      var opcionesArray = { op: "consultaConductor", usuario: userdata};

      this.api.getDespachoControlador(opcionesArray)
      .then((resultado) => {

            var dataRespond = resultado.json();

            if(dataRespond['respuesta'] == 1){

                this.usuario = dataRespond['conductor'];
                this.vehiculo = dataRespond['placa'];

            }else{

                this.app.getMostrarAlerta("Aviso","El usuario actualmente no tiene manifiestos","Aceptar")

            }

      },error => console.log(error));

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

  validaToken(push,token){

    var conexion = this.network.type;

    if (conexion != 'none'){

      var dispositivo = {uuid: this.device.uuid,plataforma: this.device.platform, modelo: this.device.model, version: this.device.version};

      var opcionesArray = { op: "validaToken", dispositivo: dispositivo, token: token, idpush: push };

      this.api.getUsuarioControlador(opcionesArray)
      .then((resultado) => {

            var dataRespond = resultado.json();

            if(dataRespond['respuesta'] == 1){

                console.log("Registro el dispositivo para push");

            }else{

                console.log("Ya tiene id para push");

            }

      },error => console.log(error));

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }


  }
}
