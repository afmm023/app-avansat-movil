import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

import { CambiapassPage } from '../cambiapass/cambiapass';
import { IntroPage } from '../intro/intro';
import { AcercadePage } from '../acercade/acercade';
import { LoginPage } from '../login/login';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/*! \fn: Modulo de configuracion de aplicativo
   * \brief: muestra opciones para la configuración del aplicativo
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/
@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {

  //variables generales
  public estado: any;

  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,
    public alerta: AlertController,
    public loader: LoadingController,
    public plataforma: Platform,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private appm: App,
    private network: Network,
    public storage: NativeStorage

  ) {
  }

  //cambia a la vista de cambio de clave
  verCambioPass(){

    this.navCtrl.push(CambiapassPage);

  }

  //cambia a la vista de información de la app
  verInfo(){

    this.navCtrl.push(AcercadePage);


  }

  salir(){

    //Crea alerta de confirmación antes de eliminar los datos de la app
    let confirm = this.alerta.create({
          title: 'Aviso',
          message: '¿Realmente quieres cerrar tu sesión?',
          buttons: [
            {
              text: 'Cancelar',
              handler: () => {

                  console.log("no elimino nada");

              }
            },
            {
              text: 'Cerrar',
              handler: () => {

                this.storage.setItem('sesion',{sesion: false});
                this.app.getNotificacion("Se ha cerrado la sesión actual","danger");
                this.appm.getRootNav().setRoot(LoginPage);

              }
            }
          ]
        });
        confirm.present();

  }


  /*! \fn: Metodo para eliminar localstorage y restablecer la app
     * \brief: le confirma al usuario si desea eliminar los datos de la app
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: Array
     * \return ninguno
  */
  eliminaData(){

    var conexion = this.network.type;

    if(conexion != 'none'){

      //Crea loader para la vista
      let load = this.loader.create({

        content: 'Eliminando...'

      });

      //Crea alerta de confirmación antes de eliminar los datos de la app
      let confirm = this.alerta.create({
            title: 'Aviso',
            message: '¿Realmente quieres eliminar tu sesión?',
            buttons: [
              {
                text: 'Cancelar',
                handler: () => {

                    console.log("no elimino nada");

                }
              },
              {
                text: 'Eliminar',
                handler: () => {

                    this.storage.getItem('usuario')
                    .then((datos) => {

                      load.present();
                      var accion = "2";
                      var opcionesArray = {op: "cambiarClave", accion: accion, usuariodata: datos};

                      this.api.getUsuarioControlador(opcionesArray)
                      .then((data) =>{

                        var dataRespond = data.json();

                        if(dataRespond['estado'] == 1){

                          this.storage.clear();
                          load.dismiss();
                          this.app.getNotificacion("Se han eliminado los datos de usuario de la aplicación","success");
                          this.appm.getRootNav().setRoot(IntroPage);

                        }else{

                          this.app.getNotificacion("Hubo un error al intentar eliminar los datos","danger");
                          load.dismiss();

                        }
                      });



                    });

                }
              }
            ]
          });
          confirm.present();

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }

}
