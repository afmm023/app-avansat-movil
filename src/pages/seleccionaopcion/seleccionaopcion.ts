import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { NativeStorage } from '@ionic-native/native-storage';


import { LoginPage } from '../login/login';
import { IntroPage } from '../intro/intro';
import { TabsPage } from '../tabs/tabs';


@IonicPage()
@Component({
  selector: 'page-seleccionaopcion',
  templateUrl: 'seleccionaopcion.html',
})
export class SeleccionaopcionPage {

  public activo: any;
  public sesion: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private nativePageTransitions: NativePageTransitions,
    private storage: NativeStorage
  ) {
  }

  ionViewWillEnter(){


    this.storage.getItem('sesion').then( sesion => {

          if(sesion['sesion'] == true){

            this.sesion = sesion;

            let options: NativeTransitionOptions = {
            direction: 'up',
            duration: 600
           };

            this.nativePageTransitions.slide(options);
            this.navCtrl.setRoot(TabsPage);


          }else{

            this.storage.getItem('activo').then(existe => {

              this.activo = existe;

            },error => console.log(error));

          }

        });

  }

  setLogin(){

    if(!this.sesion){

      if (!this.activo) {

        let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 600
       };

        this.nativePageTransitions.slide(options);
        this.navCtrl.setRoot(IntroPage);

      }

    }else{

      let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 600
     };

      this.nativePageTransitions.slide(options);
      this.navCtrl.setRoot(LoginPage);

    }

  }




}
