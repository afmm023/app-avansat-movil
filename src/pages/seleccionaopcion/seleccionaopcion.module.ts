import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeleccionaopcionPage } from './seleccionaopcion';

@NgModule({
  declarations: [
    SeleccionaopcionPage,
  ],
  imports: [
    IonicPageModule.forChild(SeleccionaopcionPage),
  ],
  exports: [
    SeleccionaopcionPage
  ]
})
export class SeleccionaopcionPageModule {}
