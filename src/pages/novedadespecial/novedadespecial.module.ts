import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovedadespecialPage } from './novedadespecial';

@NgModule({
  declarations: [
    NovedadespecialPage,
  ],
  imports: [
    IonicPageModule.forChild(NovedadespecialPage),
  ],
  exports: [
    NovedadespecialPage
  ]
})
export class NovedadespecialPageModule {}
