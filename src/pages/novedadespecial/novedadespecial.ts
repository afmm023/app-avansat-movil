import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation';
import {Camera, CameraOptions} from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { LoadingController } from 'ionic-angular';

import { DespachosPage } from '../despachos/despachos';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/**
 * Generated class for the Novedadespecial page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-novedadespecial',
  templateUrl: 'novedadespecial.html',
})
export class NovedadespecialPage {

  formnovedad: FormGroup;
  novedades = [];
  public etapa: any;
  public estado: any;
  public despacho: any;
  public novedad: any;
  public usuario: any;
  public datosNovedad: any;
  public opciones: any;
  public photos : any;
  public base64Image : string;
  public latitud: any;
  public longitud: any;
  public direccion: any;

  constructor(
    public navCtrl: NavController,
    private builder: FormBuilder,
    public api: ApiFucntionsProvider ,
    public parametrosURL: NavParams,
    public app: AppFunctionsProvider,
    private camera : Camera,
    private network: Network,
    private loader: LoadingController,
    public alertCtrl: AlertController,
    private geolocation: Geolocation
  ) {

    this.formnovedad = builder.group({
      'novedad': ['', Validators.required],
      'observacion': ['', Validators.required],
    });

  }

  ionViewWillEnter(){

      this.etapa = this.parametrosURL.get('etapa');
      this.estado = this.parametrosURL.get('estado');
      this.despacho = this.parametrosURL.get('despacho');
      this.usuario = this.parametrosURL.get('usuario');
      this.consultaNovedades(this.etapa, this.estado);
      this.photos = [];
      this.getGeolocalizacion();

  }

  deletePhoto(index) {
     let confirm = this.alertCtrl.create({
         title: 'Aviso',
         message: 'Desea eliminar la foto',
         buttons: [
           {
             text: 'No',
             handler: () => {
               console.log('Disagree clicked');
             }
           }, {
             text: 'Si',
             handler: () => {
               this.photos.splice(index, 1);
             }
           }
         ]
       });
     confirm.present();
   }

  takePhoto() {
    const options : CameraOptions = {
      quality: 30, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 1000,
      targetHeight: 1000,
      saveToPhotoAlbum: true
    }
    this.camera.getPicture(options).then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();
      }, (err) => {
        console.log(err);
      });
  }

  consultaNovedades(etapa, estado){

    var opcionesArray = {op: "getNovedadesByEtapa" , codigoEtapa: etapa, loadState: estado};

    this.api.getNovedadControlador(opcionesArray)
    .then((result) => {

          var dataRespond = result.json();
          this.novedades.length = 0;

          for(var i = 0; i < dataRespond['novedades'].length; i++){

                this.novedades.push(dataRespond['novedades'][i]);

          }

    }, error => console.log(error));


  }

  onSubmit(values){

      var conexion = this.network.type;

      if(conexion != 'none'){

        this.novedad = {
            novedad: values['novedad'],
            descripcionNovedad: values['observacion'],
            file: this.photos,
            observacion: values['observacion']
        };

        this.setRegistraNovedad(this.novedad);

      }else{

        this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

      }

  }

  setRegistraNovedad(novedad){

      let load = this.loader.create({

        content: 'Registrando...'

      });

      load.present();

      //Crea Fecha
      var f = new Date();
      let dd: number = f.getUTCDate();
      let mm: number = f.getMonth() + 1;

      if(dd < 10){

        var dia = dd.toString();

        dia = "0"+dia;

      }else{

        var dia = dd.toString();

      }

      if(mm < 10){

        var mes = String(mm);

        mes = "0"+mes;

      }

      var fecha = f.getFullYear()+"-"+mes+"-"+dia+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
      var latitud = this.latitud;
      var longitud = this.longitud;
      var direccion = this.direccion;

      var ubicacionArray = {

          fechaNovedad: fecha,
          latitud: latitud,
          longitud: longitud

      };

      this.datosNovedad = Object.assign(novedad,ubicacionArray);

      var opcionesArray = { op: "registrarNovedad" };

      var datosArray = {

          despacho: this.despacho,
          usuario: this.usuario,
          novedad: this.datosNovedad,
          sitio: direccion

      };

      this.opciones = Object.assign(opcionesArray,datosArray);

      this.api.getDespachoControlador(this.opciones)
      .then((respuesta) => {

        var dataRespond = respuesta.json();

        if(dataRespond['cod_respon'] == 1000){

          load.dismiss();
          this.app.getNotificacion("Se ha insertado la novedad", "success");
          this.navCtrl.setRoot(DespachosPage);

        }else{

            load.dismiss();
            this.app.getNotificacion("Algo salio mal", "alert");
            this.navCtrl.setRoot(DespachosPage);

        }

      }, error => console.log(error));
  }

  getGeolocalizacion(){

      this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 5000, enableHighAccuracy: true })
        .then((resp) => {

          this.latitud = resp.coords.latitude;
          this.longitud = resp.coords.longitude;

          this.app.getUbicacion(this.latitud, this.longitud)
          .then((result) => {


              this.latitud = result['latitud'];
              this.longitud = result['longitud'];
              this.direccion = result['direccion'];

          }, error => console.log(error));

        });
  }


}
