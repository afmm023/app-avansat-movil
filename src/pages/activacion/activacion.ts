import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

import {CambiaclavePage } from '../cambiaclave/cambiaclave';
import {TutorialPage } from '../tutorial/tutorial';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';

/*! \fn: Modulo de Activación de usuario
   * \brief: Activa al usuario cuando instala por primera vez la app
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/
@IonicPage()
@Component({
  selector: 'page-activacion',
  templateUrl: 'activacion.html',
})
export class ActivacionPage {

  //variables generales
  activa: FormGroup;
  public datos: any;
  public token: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loader: LoadingController,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private alertCtrl: AlertController,
    private builder: FormBuilder,
    private storage: NativeStorage,
    private network: Network,
    private device: Device
  ) {

    //Verifica que los campos del formulario no esten vacios
    this.activa = builder.group({

      'cod_usuari': ['', Validators.required],
      'cod_documento': ['', Validators.required],

    });

  }

  ionViewWillEnter(){

  }

  /*! \fn: Metodo que recibe los valores enviados desde el formualrio
     * \brief: recibe array con los valores del formulario
     * \author: Andres Felipe Muñoz
     * \date: 12/05/2017
     * \date modified: 10/06/2017
     * \param: Array
     * \return ninguno
  */
  onSubmit(datosActiva) {

    var conexion = this.network.type;

    if(conexion != 'none'){

      //Crea loader para la vista
      let load = this.loader.create({

        content: 'Activando...'

      });

      //Crea Array con la opciones que enviara a la API
      var opcionesArray = { op: "activarAplicacion",cod_uuid: this.device.uuid};

      //Array con los valores de los campos del fomulario
      var activaArray = datosActiva;

      //Union de los dos array
      this.datos = Object.assign(opcionesArray,activaArray);

      var documento= this.datos.cod_documento;

      //Muestra loader
      load.present();

      //Llama de la API Functions el metodo al que va consultar la API
      this.api.getAplicacionControlador(this.datos)
        .then((respuesta) => {

          //Almacenamos la respuesta de la petición
          var dataRespond = respuesta.json();

          switch(dataRespond['estado']){

                case(1):

                  //Alamcena los datos de la empresa en el SQLlite de la App
                  this.storage.setItem('empresa',dataRespond['empresa']);

                  //Oculta loader
                  load.dismiss();

                  //Muestra popalert
                  this.app.getNotificacion("Tu usuario ha sido activado con Éxito","success");

                  //Cambia de vista y envia como parametro el usuario, y adicional realiza una animación
                  this.navCtrl.setRoot(CambiaclavePage,{ usuario: documento }, {animate: true,animation: 'ios-transition', direction: 'forward'});

                break;
                case(2):

                  //Oculta loader
                  load.dismiss();

                  //Muestra popalert
                  this.app.getNotificacion("El Usuario ya se encuentra activo en otro Dispositivo","alert");

                  //Reinicia el formulario
                  this.activa.reset();

                break;
                case(3):

                  //Oculta loader
                  load.dismiss();

                  //Muestra popalert
                  this.app.getNotificacion("El usuario no existe, verifica e inténtalo de nuevo","danger");

                  //Reinicia el formulario
                  this.activa.reset();

                break;
          }

        }, (error) => {

          load.dismiss();

        });

    }else{

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");
      this.activa.reset();

    }

  }

  setTutorial(){

    this.navCtrl.setRoot(TutorialPage, null, {animate: true,animation: 'md-transition', direction: 'forward'});

  }



}
