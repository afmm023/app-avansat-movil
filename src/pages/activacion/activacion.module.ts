import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActivacionPage } from './activacion';

@NgModule({
  declarations: [
    ActivacionPage,
  ],
  imports: [
    IonicPageModule.forChild(ActivacionPage),
  ],
  exports: [
    ActivacionPage
  ]
})
export class ActivacionPageModule {}
