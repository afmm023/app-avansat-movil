import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';


import { EnviafacturasPage } from '../enviafacturas/enviafacturas';

/**
 * Generated class for the Facturas page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-facturas',
  templateUrl: 'facturas.html',
})
export class FacturasPage {

  public usuario: any;
  public despacho: any;
  public facturas: any;
  public manifiesto: any;
  public listaFacturas: any;
  opcionFactura = [];

  constructor(
    public navCtrl: NavController,
    private loader: LoadingController,
    public parametrosURL: NavParams,

  ) {
  }

  ionViewWillEnter(){

    let load = this.loader.create({

      content: 'Cargando...'

    });

    load.present();
    this.despacho = this.parametrosURL.get('despacho');
    this.usuario = this.parametrosURL.get('usuario');
    this.facturas = this.parametrosURL.get('facturas');
    this.manifiesto = this.parametrosURL.get('manifiesto');
    var facturasList = this.facturas.split(',');
    this.listarFacturas(facturasList);
    load.dismiss();

  }

  listarFacturas(facturas){

      this.opcionFactura.length = 0;

      for(var i = 0; i <  facturas.length ; i++){

          this.opcionFactura.push(facturas[i]);

      }

  }

  setCumplido(factura){

    this.navCtrl.push(EnviafacturasPage,{ factura: factura, usuario: this.usuario, despacho: this.despacho, manifiesto: this.manifiesto});


  }



}
