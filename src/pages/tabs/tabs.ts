import { Component } from '@angular/core';
import { NavController, AlertController, NavParams} from 'ionic-angular';

import { InicioPage } from '../inicio/inicio';
import { DespachosPage } from '../despachos/despachos';
import { ConfiguracionPage } from '../configuracion/configuracion';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InicioPage;
  tab2Root = DespachosPage;
  tab3Root = ConfiguracionPage;
  public data: any;
  valueforngif=true;

  constructor(public navCtrl: NavController, public alerta: AlertController, public parametrosURL: NavParams) {
    var parametros = parametrosURL;
    this.data = parametros;
  }

}
