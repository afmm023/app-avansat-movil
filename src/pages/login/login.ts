import { Component } from '@angular/core';
import {  NavController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { NativeStorage } from '@ionic-native/native-storage';
import { Network } from '@ionic-native/network';

import { TabsPage } from '../tabs/tabs';
import { PernotacionPage } from '../pernotacion/pernotacion';

import { ApiFucntionsProvider } from '../../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../../providers/app-functions/app-functions';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  login: FormGroup;
  private datos: any;
  public username: any;
  public type: any;

  constructor(

    public navCtrl: NavController,
    private loader: LoadingController,
    private builder: FormBuilder,
    public api: ApiFucntionsProvider,
    public app: AppFunctionsProvider,
    private alertCtrl: AlertController,
    private storage: NativeStorage,
    private network: Network,
    private device: Device

  ) {

    this.login = builder.group({
      'cod_usuari': ['', Validators.required],
      'clv_usuari': ['', Validators.required],
    });


  }

  ionViewWillEnter(){

    this.type = "password";

  }

  verPass(){

      if(this.type == "password"){

            this.type = "text";

      }else{

        this.type = "password";

      }

  }

  onSubmit(datosLogin) {

    var conexion = this.network.type;

    if(conexion != 'none'){

      let load = this.loader.create({

        content: 'Autenticando...'

      });

      this.storage.getItem('empresa')
       .then((data) => {

         var opcionesArray = { op: "validarUsuario", empresa: data, cod_uuid: this.device.uuid };

         var loginArray = datosLogin;

         this.datos = Object.assign(opcionesArray,loginArray);

         load.present();

         this.api.getUsuarioControlador(this.datos)
         .then((respuesta) => {

             var dataRespond = respuesta.json();

             if (dataRespond['login'] == 1){

               this.username = dataRespond['cod_usuari'];

               this.storage.getItem('usuario').then( result => {

                  //imprimo lo que existe
                  console.log(result);

                  //Si no hay un uaurio almacenado lo guardo
               }, error => this.storage.setItem('usuario',dataRespond));

               // Almaceno la sesion
               this.storage.setItem('sesion',{sesion: true});

               //verificamos que no se encuentr en pernotacion
               this.storage.getItem('pernotando').then(result => {

                   if(result['pernotando'] == true){

                       this.app.getMostrarAlerta("Aviso", "Debe reiniciar su ruta actual","Aceptar");
                       this.navCtrl.setRoot(PernotacionPage,{},{ animate: true, direction: 'forward'});

                   }
               }, error => this.navCtrl.setRoot(TabsPage,{data: dataRespond},{animate: true, direction: 'forward'}));

               load.dismiss();


             }else{

               load.dismiss();

               this.app.getMostrarAlerta("Aviso","Usuario no válido","Aceptar");

               this.login.reset();

             }

         }, (error) => console.log(error));

       });

    }else {

      this.app.getNotificacion("Actualmente no tiene una conexión a internet, intentalo más tarde","danger");

    }

  }


}
