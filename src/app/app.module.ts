import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import { TabsPage } from '../pages/tabs/tabs';
import { ActivacionPage } from '../pages/activacion/activacion';
import { AcercadePage } from '../pages/acercade/acercade';
import { DespachosPage } from '../pages/despachos/despachos';
import { InicioPage } from '../pages/inicio/inicio';
import { LoginPage } from '../pages/login/login';
import { PrincipalPage } from '../pages/principal/principal';
import { SeleccionaopcionPage } from '../pages/seleccionaopcion/seleccionaopcion';
import { EtapadespachoPage } from '../pages/etapadespacho/etapadespacho';
import { CambiaclavePage } from '../pages/cambiaclave/cambiaclave';
import { IntroPage } from '../pages/intro/intro';
import { EtapacarguePage } from '../pages/etapacargue/etapacargue';
import { EtapatransitoPage } from '../pages/etapatransito/etapatransito';
import { EtapadescarguePage } from '../pages/etapadescargue/etapadescargue';
import { EtapacumplidosPage } from '../pages/etapacumplidos/etapacumplidos';
import { PernotacionPage } from '../pages/pernotacion/pernotacion';
import { NovedadespecialPage } from '../pages/novedadespecial/novedadespecial';
import { DestinatarioPage } from '../pages/destinatario/destinatario';
import { FacturasPage } from '../pages/facturas/facturas';
import { EnviafacturasPage } from '../pages/enviafacturas/enviafacturas';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { CambiapassPage } from '../pages/cambiapass/cambiapass';

import { ActivacionPageModule } from '../pages/activacion/activacion.module';
import { AcercadePageModule } from '../pages/acercade/acercade.module';
import { CambiaclavePageModule } from '../pages/cambiaclave/cambiaclave.module';
import { LoginPageModule } from '../pages/login/login.module';
import { DespachosPageModule } from '../pages/despachos/despachos.module';
import { InicioPageModule } from '../pages/inicio/inicio.module';
import { PrincipalPageModule } from '../pages/principal/principal.module';
import { SeleccionaopcionPageModule } from '../pages/seleccionaopcion/seleccionaopcion.module';
import { EtapadespachoPageModule } from '../pages/etapadespacho/etapadespacho.module';
import { IntroPageModule } from '../pages/intro/intro.module';
import { EtapacarguePageModule } from '../pages/etapacargue/etapacargue.module';
import { EtapatransitoPageModule } from '../pages/etapatransito/etapatransito.module';
import { EtapadescarguePageModule } from '../pages/etapadescargue/etapadescargue.module';
import { EtapacumplidosPageModule } from '../pages/etapacumplidos/etapacumplidos.module';
import { PernotacionPageModule } from '../pages/pernotacion/pernotacion.module';
import { NovedadespecialPageModule } from '../pages/novedadespecial/novedadespecial.module';
import { DestinatarioPageModule } from '../pages/destinatario/destinatario.module';
import { FacturasPageModule } from '../pages/facturas/facturas.module';
import { EnviafacturasPageModule } from '../pages/enviafacturas/enviafacturas.module';
import { TutorialPageModule } from '../pages/tutorial/tutorial.module';
import { CambiapassPageModule } from '../pages/cambiapass/cambiapass.module';
import { ConfiguracionPageModule } from '../pages/configuracion/configuracion.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Device } from '@ionic-native/device';
import { Toast } from '@ionic-native/toast';
import { Network } from '@ionic-native/network';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { OneSignal } from '@ionic-native/onesignal';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { DatePicker } from '@ionic-native/date-picker';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { NativeStorage } from '@ionic-native/native-storage';
import { SQLite } from '@ionic-native/sqlite';
import { AppVersion } from '@ionic-native/app-version';
import { Keyboard } from '@ionic-native/keyboard';

import { ApiFucntionsProvider } from '../providers/api-fucntions/api-fucntions';
import { AppFunctionsProvider } from '../providers/app-functions/app-functions';
import { BdProvider } from '../providers/bd/bd';

@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,
      {
          platforms : {
            ios : {
              scrollPadding: false,
              scrollAssist: true,
              autoFocusAssist: true,
              backButtonText: 'Atrás'
            },
            android:{
              scrollPadding: false,
              scrollAssist: true,
              autoFocusAssist: true,
              tabsHideOnSubPages: true,
            }
          }
        }),
    ActivacionPageModule,
    AcercadePageModule,
    LoginPageModule,
    DespachosPageModule,
    InicioPageModule,
    PrincipalPageModule,
    SeleccionaopcionPageModule,
    EtapadespachoPageModule,
    CambiaclavePageModule,
    IntroPageModule,
    EtapacarguePageModule,
    EtapatransitoPageModule,
    EtapadescarguePageModule,
    EtapacumplidosPageModule,
    PernotacionPageModule,
    NovedadespecialPageModule,
    DestinatarioPageModule,
    FacturasPageModule,
    EnviafacturasPageModule,
    ConfiguracionPageModule,
    CambiapassPageModule,
    TutorialPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DespachosPage,
    InicioPage,
    LoginPage,
    PrincipalPage,
    SeleccionaopcionPage,
    EtapadespachoPage,
    ActivacionPage,
    AcercadePage,
    CambiaclavePage,
    IntroPage,
    EtapacarguePage,
    EtapatransitoPage,
    EtapadescarguePage,
    EtapacumplidosPage,
    PernotacionPage,
    DestinatarioPage,
    NovedadespecialPage,
    FacturasPage,
    EnviafacturasPage,
    ConfiguracionPage,
    TutorialPage,
    CambiapassPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    Toast,
    Network,
    Camera,
    Geolocation,
    LocalNotifications,
    OneSignal,
    NativePageTransitions,
    DatePicker,
    NativeGeocoder,
    NativeStorage,
    SQLite,
    AppVersion,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiFucntionsProvider,
    AppFunctionsProvider,
    BdProvider
  ]
})
export class AppModule {}
