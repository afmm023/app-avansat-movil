import { Component } from '@angular/core';
import { Platform , ModalController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';
import { SQLite } from '@ionic-native/sqlite';
import { Keyboard } from '@ionic-native/keyboard';


import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage } from '../pages/intro/intro';

import { BdProvider } from '../providers/bd/bd';


/*! \fn: Modulo Root
   * \brief: Carga toda la configuraci贸n de la app
   * \author: Andres Felipe Muñoz
   * \date: 12/05/2017
   * \date modified: 10/06/2017
   * \param: ninguno
   * \return ninguno
*/

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public sqlite: SQLite,
    public storage: NativeStorage,
    public bd: BdProvider,
    modalCtrl: ModalController,
    public keyboard: Keyboard
  ) {

    platform.ready().then(() => {

      statusBar.styleDefault();
      this.createDatabase();

      this.keyboard.hideKeyboardAccessoryBar(false);
      this.keyboard.disableScroll(true);

    });
  }

  getValidaSesion(){

    this.storage.getItem('activo').then( activo => {

        console.log(activo);

        if( activo['activo'] == true ){

            this.storage.getItem('sesion').then(sesion => {

              if(sesion['sesion'] == true){

                this.rootPage = TabsPage;

              }else{

                this.rootPage = LoginPage;


              }

            }, error => this.rootPage = LoginPage );

        }else{

          this.rootPage = IntroPage;


        }

    },error => this.rootPage = IntroPage);
  }

  private createDatabase(){
    this.sqlite.create({
      name: 'avansat.db',
      location: 'default' // the location field is required
    })
    .then((db) => {
      this.bd.setDatabase(db);
      console.log(db);
      return this.bd.createTable();
    })
    .then(() =>{
      this.splashScreen.hide();
      this.getValidaSesion();
    })
    .catch(error =>{
      console.error(error);
    });
  }
}
