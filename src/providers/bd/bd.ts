import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class BdProvider {

  db: SQLiteObject = null;

  constructor() {

  }

  setDatabase(db: SQLiteObject){

    if(this.db === null){
      this.db = db;
    }

  }

  create(novedad: any){

    let sql = 'INSERT INTO novedades(vista, contenido, fecha, novedad, despacho) VALUES(?,?,?,?,?)';

    return this.db.executeSql(sql, [novedad['vista'], novedad['contenido'], novedad['fecha'], novedad['novedad'], novedad['despacho']]);

  }

  createTable(){

    let sql = 'CREATE TABLE IF NOT EXISTS novedades(id INTEGER PRIMARY KEY AUTOINCREMENT, vista TEXT, contenido TEXT, fecha TEXT, novedad TEXT, despacho TEXT)';

    return this.db.executeSql(sql, []);

  }

  deleteAll(){

    let sql = 'DELETE FROM novedades WHERE 1';

    return this.db.executeSql(sql, []);

  }

  getAll(){

    let sql = 'SELECT * FROM novedades';

    return this.db.executeSql(sql, [])
    .then(response => {

      let novedades = [];

      for (let index = 0; index < response.rows.length; index++) {

        novedades.push( response.rows.item(index) );

      }

      return Promise.resolve( novedades );

    })

    .catch(error => Promise.reject(error));

  }

}
