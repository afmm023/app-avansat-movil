import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { NativeGeocoder, NativeGeocoderReverseResult} from '@ionic-native/native-geocoder';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class AppFunctionsProvider {

  constructor(
    public http: Http,
    private alertCtrl: AlertController,
    public toastCtrl: Toast,
    private nativeGeocoder: NativeGeocoder
  ) {

  }

  //------------------------- FUNCION DE ALERTA -----

  getMostrarAlerta(titulo,contenido,boton){

    //Creamos Alerta
    let alert = this.alertCtrl.create({
      title: titulo,
      message: contenido,
      buttons: [boton]
    });
    //Mostramos Alerta
    alert.present();

  }

  //------------------------- FUNCION DE NOTIFICACION _____

  getNotificacion(titulo, estilo){

      var color = "";

      if(estilo == "success"){

        color = '#008000';

      }else if(estilo == "warning"){

        color = '#FFA500';

      }else{

        color = '#FF0000';

      }

      this.toastCtrl.showWithOptions({
        message: titulo,
        duration:3000,
        position:"top",
        styling: {
          backgroundColor: color
        }
      }).subscribe(
          toast => {
              console.log(toast);
      });

  }

  //-------------------------- FUNCION DE UBICACION ----
  getUbicacion(latitud,longitud){

    return this.nativeGeocoder.reverseGeocode(latitud, longitud)
      .then(
        (result: NativeGeocoderReverseResult) =>
          {

            var direccion = result.countryName+"-"+result.city+","+ result.street+","+result.houseNumber;

            let ubicacionArray = {

                latitud: latitud,
                longitud: longitud,
                direccion: direccion

            };

            console.log(result);


            return ubicacionArray;

          })
      .catch((error: any) => console.log(error));
  }

}
