import { Injectable } from '@angular/core';
import { Http , Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiFucntionsProvider {

  //Tener en cuenta la URL la version de API que trabajara

  //Apiurl = 'https://dev.intrared.net:8083/ap/interf/app/apispg/V2/controlador/';
  Apiurl = 'https://central.intrared.net/ap/interf/apispg/V2/controlador/';

  constructor(public http: Http) {

  }

  getUsuarioControlador(contentArray){

    var headers = new Headers();
    headers.append('Content-Type', 'application/json' );

    let cabeceras = new RequestOptions({ headers: headers });

    let opcionArray = contentArray;

    return this.http.post(this.Apiurl+'UsuarioControlador.php', opcionArray , cabeceras)
        .map(data => {
              var dataapi= data;
              return dataapi;
         }, error => {

          console.log(error);

    }).toPromise();

  }

  getDespachoControlador(contentArray){

    var headers = new Headers();
    headers.append('Content-Type', 'application/json' );

    let cabeceras = new RequestOptions({ headers: headers });


    let opcionArray = contentArray;

    return this.http.post(this.Apiurl+'DespachoControlador.php', opcionArray , cabeceras)
        .map(data => {
              var dataapi= data;
              return dataapi;
         }, error => {

          console.log(error);

    }).toPromise();

  }

  getAplicacionControlador(contentArray){

    var headers = new Headers();
    headers.append('Content-Type', 'application/json' );

    let cabeceras = new RequestOptions({ headers: headers });

    let opcionArray = contentArray;

    return this.http.post(this.Apiurl+'AplicacionControlador.php', opcionArray , cabeceras)
        .map(data => {
              var dataapi= data;
              return dataapi;
         }, error => {

          console.log(error);

    }).toPromise();

  }
  getNovedadControlador(contentArray){

    var headers = new Headers();
    headers.append('Content-Type', 'application/json' );

    let cabeceras = new RequestOptions({ headers: headers });

    let opcionArray = contentArray;

    return this.http.post(this.Apiurl+'NovedadControlador.php', opcionArray , cabeceras)
        .map(data => {
              var dataapi= data;
              return dataapi;
         }, error => {

          console.log(error);

    }).toPromise();

  }

}
